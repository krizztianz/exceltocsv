﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ExcelToCSV
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void generateCSV_Click(object sender, EventArgs e)
        {
            if (uplFile.HasFile)
            {
                var file = uplFile.PostedFile;
                var filename = file.FileName;
                var arrext = filename.Split('.').ToArray();
                var ext = arrext.LastOrDefault();
                String path = Server.MapPath(@"~/uploadedFile/" + file.FileName);
                String connString = "";
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                string filePath = "";
                string outputPath = "";

                try
                {
                    if (ext == "xls")
                    {
                        connString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + path + ";Extended Properties=\"Excel 8.0;HDR=No;IMEX=1\"";
                    }
                    else if (ext == "xlsx")
                    {
                        connString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\"";
                    }
                    else
                    {
                        throw new Exception("File type not supported!");
                    }

                    file.SaveAs(path);

                    string query = "SELECT * FROM [Sheet1$]";
                    using (OleDbConnection connection = new OleDbConnection(connString))
                    {
                        OleDbCommand command = new OleDbCommand(query, connection);

                        connection.Open();
                        OleDbDataReader reader = command.ExecuteReader();

                        if (reader.HasRows)
                        {
                            dt.Load(reader);
                        }
                        else
                        {
                            throw new Exception("File does not contain any data!");
                        }

                        reader.Close();
                    }

                    int fieldcount = dt.Columns.Count;
                    int rowlimit = (dt.Rows.Count - 1);
                    int row = 0;
                    foreach (DataRow item in dt.Rows)
                    {
                        List<String> sa = new List<String>() { };
                        for (int i = 0; i < fieldcount; i++)
                        {
                            var val1 = item[i].ToString();
                            sa.Add(item[i].ToString());
                        }

                        if (row < rowlimit)
                        {
                            sb.AppendLine(String.Join(",", sa.ToArray()));
                        }
                        else
                        {
                            sb.Append(String.Join(",", sa.ToArray()));
                        }

                        row++;
                    }

                    filePath = @"~/generatedFile/";
                    outputPath = Server.MapPath(filePath + "generatedCsv.csv");

                    //stringbuilder to physical csv file
                    using (StreamWriter swriter = new StreamWriter(outputPath))
                    {
                        swriter.Write(sb.ToString());
                    }

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.ContentType = "text/plain";
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Path.GetFileName(outputPath));
                    HttpContext.Current.Response.WriteFile(outputPath);
                    HttpContext.Current.Response.End();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    if (!String.IsNullOrEmpty(path) && File.Exists(path))
                    {
                        File.Delete(path);
                    }

                    //if (!String.IsNullOrEmpty(outputPath) && File.Exists(outputPath))
                    //{
                    //    File.Delete(outputPath);
                    //}
                }
            }
            else
            {
                throw new Exception("No file uploaded!");
            }
        }
    }
}